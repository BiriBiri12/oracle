import requests
import json
import datetime
import caldav
from caldav.elements import dav, cdav
import icalendar
import urllib
import subprocess
import re

matchSummary = re.compile(r"SUMMARY:.+?\\r")

class zeus:
    raining = False
    temp = 0
    maxTemp = 0
    minTemp = 0
    def toFahrenheit(self,i):
        i = (i - 273.15)*1.8 + 32.00 ## Convert kelvin to fahgrenheit
        return i
    def refresh(self,apikey):
        now = datetime.datetime.now()

        # This stores the url
        base_url = "http://api.openweathermap.org/data/2.5/forecast?"

        # Enter city ID here
        city_id = "4351977"

        # This is final url. This is concatenation of base_url, API_key and city_id
        Final_url = base_url + "appid=" + apikey + "&id=" + city_id
        # this variable contain the JSON data which the API returns
        weather_data = requests.get(Final_url).json()

        relevantPredictions = []
        i = 0
        for d in weather_data["list"]:
            date = d["dt_txt"].split(" ")[0].split("-") ## Find the date in the weather data

            if (now.day == int(date[2])):
                relevantPredictions.append(i)
            i += 1
        for i in relevantPredictions: ## Test if it's raining
            if (weather_data["list"][i]["weather"][0]["main"] == "Rain"):
                self.raining = True
            else:
                self.raining = False
        # Get the true max and min temps
        maxVMin = [[0,"temp_max",True], [20000000,"temp_min",False]]
        for i in maxVMin:
            for x in relevantPredictions:
                new = weather_data["list"][x]["main"][i[1]] ## Get either the min or max temperature at a certain time
                if (i[2]):
                    if (new > i[0]):
                        i[0] = new
                else:
                    if (new < i[0]):
                        i[0] = new
        self.maxTemp = self.toFahrenheit(maxVMin[0][0])
        self.minTemp = self.toFahrenheit(maxVMin[1][0])

    def getRaining(self):
        return self.raining
    def getMinTemp(self):
        return self.minTemp
    def getMaxTemp(self):
        return self.maxTemp

class calendar:
    def __init__(self,host,user,password):
        self.link = "https://"+user+":"+password+"@"+host
        self.client = caldav.DAVClient(self.link)
        self.principal = self.client.principal()
        self.calendars = self.principal.calendars()
    def getName(self,url,user,password): # Get the summary of a given event
        url = "https://"+user+":"+password+"@"+str(url)[15:] #Generate url of event with username and password
        text = subprocess.run(["curl","-s",url],stdout = subprocess.PIPE) # Get the text of the events
        name = matchSummary.search(str(text)).group() # Find the summary
        name = name[8:-2] #Edit the text to make it readable
        if (name == "Eleanor Busy"):
            return ""
        else:
            return name
    def getToday(self,user,password):
        events = []
        for i in self.calendars:
            now = datetime.datetime.now()
            results = i.date_search(now, now + datetime.timedelta(days=1)) #Get the events today
            for event in results:
                events.append(self.getName(event,user,password))
        return(events)
