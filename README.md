# Oracle.py

## What is it

Oracle.py is a python script to generate a daily "forecast", that would be sent in the morning by some server. 

## Usage

Add your nextcloud username and password, as well as an open weather maps API key to the config file. This is neccesary. You will also have to mess with the nextcloud URL at the start of oracle.py (to be fixed later). Then, simply run ```./run.sh```

## Dependencies

Oracle.py requires python3 and the following python libraries, which can be installed via pip:

* requests
* caldav
* icalendar


