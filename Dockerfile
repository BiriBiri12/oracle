FROM python:3
RUN pip3 install icalendar caldav
ADD . /tmp/oracle
WORKDIR /tmp/oracle
CMD [ "python3", "./oracle.py"]
